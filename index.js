const channelBot = require("channel-telegram-bot");
const Tortoise = require('tortoise');
const rabbitHost = process.env.RABBIT_HOST || "amqp://guest:guest@localhost:5672/";
const chatID = process.env.TELEGRAM_CHAT_ID || "";
const botID = process.env.TELEGRAM_BOT_ID || "";
const queue = process.env.MESSAGE_QUEUE || "job-log";

const tortoise = new Tortoise(rabbitHost)

tortoise.on(Tortoise.EVENTS.CONNECTIONCLOSED, () => {
    console.log('RabbitMQ connection closed')
})

tortoise.on(Tortoise.EVENTS.CONNECTIONDISCONNECTED, () => {
    console.log('RabbitMQ connection disconnected')
})

tortoise.on(Tortoise.EVENTS.CONNECTIONERROR, (err) => {
    console.log('RabbitMQ connection error', err)
})

tortoise
    .queue(queue, { durable: false })
    .prefetch(1)
    .json()
    .subscribe((msg, ack, nack) => {

        console.log(msg)
        channelBot
            .sendMessage(
                chatID,
                msg.message,
                botID
            )
            .catch(err => console.log(err) && nack()).then(ack());
    })